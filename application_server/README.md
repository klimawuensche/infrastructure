# CI server

The [terraform](terraform) directory contains the Terraform definitions for an instance intended as an application server for the project.
The [ansible](ansible) directory contains an Ansible playbook which configures the instance (but does not yet run the application).
In order to actually deploy the application, you need to trigger a build in the [website](https://gitlab.com/klimawuensche/website) project.

## Applying the configuration

If you want to start from scratch, or ensure that the current configuration is applied, just run
```shell
make apply
```