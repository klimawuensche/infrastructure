resource "hcloud_server" "klima_instance" {
  name = "app-${count.index}"
  image = var.klima_instance_image
  server_type = var.klima_instance_server_type
  ssh_keys = var.klima_instance_ssh_keys
  location = var.hcloud_location
  user_data = templatefile("${path.module}/user_data.tpl", {username = "klima"})
  backups = false
  firewall_ids = [hcloud_firewall.firewall.id]
  keep_disk = true

  labels = {
    Name = "app-${count.index}"
    AppServerId = count.index
    Role = "application_server"
  }

  count = var.server_count
}

resource "hcloud_firewall" "firewall" {
  name = "klima_firewall"

  rule {
    direction = "in"
    protocol = "tcp"
    port = "22"
    source_ips = [
      "0.0.0.0/0",
      "::/0"
    ]
  }

  rule {
    direction = "in"
    protocol = "tcp"
    port = "80"
    source_ips = [
      "10.0.0.0/24"
    ]
  }

}
