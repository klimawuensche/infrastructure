output "public_ipv4" {
  value = zipmap(hetznerdns_record.app.*.name, hcloud_server.klima_instance.*.ipv4_address)
}

output "public_ipv6" {
  value = zipmap(hetznerdns_record.app.*.name, hcloud_server.klima_instance.*.ipv6_address)
}

output "private_ipv4" {
  value = zipmap(hetznerdns_record.app.*.name, hcloud_server_network.srvnetwork.*.ip)
}