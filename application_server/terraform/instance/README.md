# Data volume

This Terraform module provisions:
- a virtual server instance
- a firewall allowing only SSH, HTTP and HTTPS
- a volume attachment for the data volume

Apply the configuration using the CI pipelines.

## Configuration

The configuration variables reside in [vars.tf](vars.tf).