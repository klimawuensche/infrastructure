data "hcloud_network" "private" {
  name = "network"
}

resource "hcloud_server_network" "srvnetwork" {
  server_id = hcloud_server.klima_instance[count.index].id
  network_id = data.hcloud_network.private.id

  count = var.server_count
  depends_on = [hcloud_server.klima_instance]
}