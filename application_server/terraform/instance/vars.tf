variable "klima_instance_image" {
  description = "The OS image of the klima instance"
  default = "ubuntu-20.04"
}

variable "klima_instance_server_type" {
  description = "The server type of the klimawuensche application instances"
  default = "cx11"
}

variable "klima_instance_ssh_keys" {
  description = "The names of the SSH keys to inject into the klima instance"
  default = ["diskordanz", "mgottschalk@gmx.net", "marco@ino.io", "ci-server"]
}

variable "hcloud_location" {
  description = "The location to use for the infrastructure components"
  default = "nbg1"
}

variable "server_count" {
  description = "How many application servers to create"
  type = number
  default = 2

  validation {
    condition = var.server_count <= 4
    error_message = "We only have datadog licenses for 5 hosts, i.e. we may only have 4 app servers + 1 database server."
  }

  validation {
    condition = var.server_count >= 1
    error_message = "We need at least one application server."
  }
}
