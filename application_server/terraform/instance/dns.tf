data "hetznerdns_zone" "klimawuensche_de" {
  name = "klimawuensche.de"
}

resource "hetznerdns_record" "app" {
  zone_id = data.hetznerdns_zone.klimawuensche_de.id
  name = hcloud_server.klima_instance[count.index].labels.Name
  value = hcloud_server.klima_instance[count.index].ipv4_address
  type = "A"
  ttl= 300

  count = var.server_count
}