provider "hcloud" {
}

provider "hetznerdns" {
}

terraform {
  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "~> 1.26"
    }
    hetznerdns = {
      source = "timohirt/hetznerdns"
      version = "1.2.0"
    }
  }
  backend "http" {
  }
}
