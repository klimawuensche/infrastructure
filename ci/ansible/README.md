# CI runner

This directory contains two playbooks:
- `main.yml` provisions CI instances and registers runners
- `unregister.yml` unregisters runners.

Invoking these playbooks should be done via the supplied [Makefile](Makefile), as it takes care of necessary variables:
```shell
# Provision the runner instance
make apply
# Run the provisioning with --check flag
make check
# Only run the gitlab-runner part of the playbook
make apply-runner

# Only unregister the runner
make unregister-runner
```