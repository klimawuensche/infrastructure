data "hcloud_network" "private" {
  name = "network"
}

resource "hcloud_server_network" "db" {
  server_id = hcloud_server.ci.id
  network_id = data.hcloud_network.private.id
}