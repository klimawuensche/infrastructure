resource "hcloud_server" "ci" {
  name = "ci"
  image = var.ci_instance_image
  server_type = var.ci_instance_server_type
  ssh_keys = var.ci_instance_ssh_keys
  location = var.hcloud_location
  user_data = templatefile("${path.module}/user_data.tpl", {username = "klima"})
  backups = false
  firewall_ids = [hcloud_firewall.firewall.id]
  keep_disk = true

  labels = {
    Name = "ci"
  }
}

resource "hcloud_firewall" "firewall" {
  name = "ci_firewall"

  rule {
    direction = "in"
    protocol = "tcp"
    port = "22"
    source_ips = [
      "0.0.0.0/0",
      "::/0"
    ]
  }
}