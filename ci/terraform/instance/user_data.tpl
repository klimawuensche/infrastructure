#!/bin/bash
set -euo pipefail

# Create user
userhome=/home/${username}
useradd -d $${userhome} -s /bin/bash ${username}

# Make available via SSH
mkdir -p $${userhome}/.ssh
chmod 700 $${userhome}/.ssh
cp /root/.ssh/authorized_keys $${userhome}/.ssh/
chmod 600 $${userhome}/.ssh/authorized_keys
chown -R ${username} $${userhome}

# Grant passwordless sudo
sudofile="99-passwordless-user"
echo "${username} ALL=(ALL) NOPASSWD:ALL" | (sudo su -c "EDITOR='tee' visudo -f /etc/sudoers.d/$${sudofile}")