output "public_ipv4" {
  value = hcloud_server.ci.ipv4_address
}

output "public_ipv6" {
  value = hcloud_server.ci.ipv6_address
}