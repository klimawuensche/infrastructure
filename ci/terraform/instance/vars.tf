variable "ci_instance_image" {
  description = "The OS image of the CI instance"
  default = "ubuntu-20.04"
}

variable "ci_instance_server_type" {
  description = "The server type of the CI instance"
  default = "cx21"
}

variable "ci_instance_ssh_keys" {
  description = "The names of the SSH keys to inject into the klima instance"
  default = ["diskordanz", "mgottschalk@gmx.net", "marco@ino.io"]
}
