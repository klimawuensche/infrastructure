# CI server

The [terraform](terraform) directory contains the Terraform definitions for an instance intended as a CI server for the project.
The [ansible](ansible) directory contains an Ansible playbook which configures the instance and registers a runner at the
group level.

## Applying the configuration

If you want to start from scratch, or ensure that the current configuration is applied, just run
```shell
make apply
```
In order to unregister the runner and destroy the instance, run
```shell
make destroy
```