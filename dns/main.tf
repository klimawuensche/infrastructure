resource "hetznerdns_zone" "klimawuensche_de" {
  name = "klimawuensche.de"
  ttl = 86400

  lifecycle {
    prevent_destroy = true
  }
}

resource "hetznerdns_zone" "klimawuensche_de_umlaut" {
  name = "xn--klimawnsche-yhb.de"  # klimawünsche.de
  ttl = 86400

  lifecycle {
    prevent_destroy = true
  }
}

resource "hetznerdns_zone" "klimawuensche_org_umlaut" {
  name = "xn--klimawnsche-yhb.org"  # klimawünsche.org
  ttl = 86400

  lifecycle {
    prevent_destroy = true
  }
}

resource "hetznerdns_zone" "klimawuensche_org" {
  name = "klimawuensche.org"
  ttl = 86400

  lifecycle {
    prevent_destroy = true
  }
}

resource "hetznerdns_zone" "klimawuensche_net" {
  name = "klimawuensche.net"
  ttl = 86400

  lifecycle {
    prevent_destroy = true
  }
}

resource "hetznerdns_zone" "klimabaender_de" {
  name = "klimabaender.de"
  ttl = 86400

  lifecycle {
    prevent_destroy = true
  }
}

resource "hetznerdns_zone" "klimabaender_de_umlaut" {
  name = "xn--klimabnder-v5a.de"  # klimabänder.de
  ttl = 86400

  lifecycle {
    prevent_destroy = true
  }
}

resource "hetznerdns_zone" "klimabaender_com" {
  name = "klimabaender.com"
  ttl = 86400

  lifecycle {
    prevent_destroy = true
  }
}

resource "hetznerdns_zone" "klimabaender_com_umlaut" {
  name = "xn--klimabnder-v5a.com"  # klimabänder.com
  ttl = 86400

  lifecycle {
    prevent_destroy = true
  }
}

resource "hetznerdns_zone" "klimabaender_org_umlaut" {
  name = "xn--klimabnder-v5a.org"  # klimabänder.org
  ttl = 86400

  lifecycle {
    prevent_destroy = true
  }
}

locals {
  zones = [
    hetznerdns_zone.klimawuensche_de,
    hetznerdns_zone.klimawuensche_de_umlaut,
    hetznerdns_zone.klimawuensche_org,
    hetznerdns_zone.klimawuensche_org_umlaut,
    hetznerdns_zone.klimawuensche_net,
    hetznerdns_zone.klimabaender_de,
    hetznerdns_zone.klimabaender_de_umlaut,
    hetznerdns_zone.klimabaender_com,
    hetznerdns_zone.klimabaender_com_umlaut,
    hetznerdns_zone.klimabaender_org_umlaut,
  ]
  zones_by_name = {for zone in local.zones: zone.name => zone}
}
