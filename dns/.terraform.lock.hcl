# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/timohirt/hetznerdns" {
  version     = "1.2.0"
  constraints = "1.2.0"
  hashes = [
    "h1:4c5OctdietH//b/7MiyDTOOYNK8LaaDLhNoTw4JsEe8=",
    "h1:HFSfVX2yoeF0Zv9+Yh1TVgBu6TqpvSd9b5UuTIfTOvE=",
    "h1:J3xGHuN9jXCOjwmdWBXhjkUHP3DdovjzwpUwbEvM5GU=",
    "h1:P0fTzuBQf5dDVDv0D+u2g9HNXKGwCPi3TT+8ELvzZrA=",
    "h1:eeWcZfXY4Aqj+6FZoJHTVx5ombEL9ncWn/1S0muWf1Q=",
    "h1:lu0RZ+1RFH1j14NXfl/gBDPv4yW2ExoUeGUqKT9QD7g=",
    "h1:tZ31LvG/IMBhjx5PDnBHz4AsRf7tEW1iglh+Asy3NnM=",
    "h1:uzMH8VxS1WhKVK5l+jCGj/aJhIcvnj69DQqh1MKV8FQ=",
  ]
}
