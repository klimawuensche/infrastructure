# Hack: Some of our zones were created via the web UI and have been allocated NS records
# which are not visible in the API. These would then have duplicate NS records.
# Other zones were created using the API, and did not get NS records allocated. 
# This means that we only need NS records for a certain subset of our zones.

resource "hetznerdns_record" "ns1" {
  zone_id = each.key
  name = "@"
  value = "hydrogen.ns.hetzner.com."
  type = "NS"
  ttl = 86400

  for_each = toset(local.zones_without_native_ns_records.*.id)
}

resource "hetznerdns_record" "ns2" {
  zone_id = each.key
  name = "@"
  value = "helium.ns.hetzner.de."
  type = "NS"
  ttl = 86400

  for_each = toset(local.zones_without_native_ns_records.*.id)
}

resource "hetznerdns_record" "ns3" {
  zone_id = each.key
  name = "@"
  value = "oxygen.ns.hetzner.com."
  type = "NS"
  ttl = 86400

  for_each = toset(local.zones_without_native_ns_records.*.id)
}

locals {
  zones_without_native_ns_records = [
    hetznerdns_zone.klimawuensche_de,
    hetznerdns_zone.klimawuensche_de_umlaut,
    hetznerdns_zone.klimawuensche_org,
    hetznerdns_zone.klimawuensche_org_umlaut,
    hetznerdns_zone.klimawuensche_net,
    hetznerdns_zone.klimabaender_de_umlaut,
    hetznerdns_zone.klimabaender_org_umlaut,
  ]
}
