# This record enables us to address images stored in our image bucket at Strato HiDrive via cdn.klimabaender.de.
# It also allows keycdn to generate a TLS certificate for this domain name.
resource "hetznerdns_record" "keycdn" {
  zone_id = hetznerdns_zone.klimabaender_de.id
  name = "cdn"
  value = "klimabaender-197ec.kxcdn.com."
  type = "CNAME"
  ttl= 600
}

resource "hetznerdns_record" "keycdn_dev" {
  zone_id = hetznerdns_zone.klimabaender_de.id
  name = "devcdn"
  value = "klimabaenderdev-197ec.kxcdn.com."
  type = "CNAME"
  ttl= 600
}

