resource "hetznerdns_record" "mx" {
  zone_id = each.value
  name    = "@"
  value   = "5 smtpin.rzone.de."
  type    = "MX"
  ttl     = 600

  for_each = toset([for zone in local.strato_mail_zones: zone.id])
}

locals {
  strato_mail_zones = [
    hetznerdns_zone.klimawuensche_de,
    hetznerdns_zone.klimawuensche_de_umlaut,
    hetznerdns_zone.klimawuensche_org,
    hetznerdns_zone.klimawuensche_org_umlaut,
    hetznerdns_zone.klimawuensche_net,
    hetznerdns_zone.klimabaender_de_umlaut,
    hetznerdns_zone.klimabaender_com,
    hetznerdns_zone.klimabaender_com_umlaut,
    hetznerdns_zone.klimabaender_org_umlaut,
  ]
  mail_egress_verification = {
    klimabaender_de = {
      zone  = hetznerdns_zone.klimabaender_de
      dkim  = "k=rsa;p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDeMVIzrCa3T14JsNY0IRv5/2V1/v2itlviLQBwXsa7shBD6TrBkswsFUToPyMRWC9tbR/5ey0nRBH0ZVxp+lsmTxid2Y2z+FApQ6ra2VsXfbJP3HE6wAO0YTVEJt1TmeczhEd2Jiz/fcabIISgXEdSpTYJhb0ct0VJRxcg4c8c7wIDAQAB"
      spf   = "v=spf1 include:spf.sendinblue.com include:_spf.mailfence.com mx ~all"
      dmarc = "v=DMARC1; p=none; sp=none; rua=mailto:reports@klimabaender.de!10m; ruf=mailto:reports@klimabaender.de!10m; rf=afrf; pct=100; ri=86400"
    },
    klimawuensche_de = {
      zone  = hetznerdns_zone.klimawuensche_de
      dkim  = "k=rsa;p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDeMVIzrCa3T14JsNY0IRv5/2V1/v2itlviLQBwXsa7shBD6TrBkswsFUToPyMRWC9tbR/5ey0nRBH0ZVxp+lsmTxid2Y2z+FApQ6ra2VsXfbJP3HE6wAO0YTVEJt1TmeczhEd2Jiz/fcabIISgXEdSpTYJhb0ct0VJRxcg4c8c7wIDAQAB"
      spf   = "v=spf1 include:spf.sendinblue.com include:_spf.mailfence.com mx ~all"
      dmarc = "v=DMARC1; p=none; sp=none; rua=reports@klimabaender.de!10m; ruf=mailto:reports@klimabaender.de!10m; rf=afrf; pct=100; ri=86400"
    }
  }
}

resource "hetznerdns_record" "dkim" {
  zone_id = each.value.zone.id

  name  = "mail._domainkey"
  value = "\"${each.value.dkim}\""
  type  = "TXT"
  ttl   = 600

  for_each = local.mail_egress_verification
}

resource "hetznerdns_record" "dmarc" {
  zone_id = each.value.zone.id

  name  = "_dmarc"
  value = "\"${each.value.dmarc}\""
  type  = "TXT"
  ttl   = 600

  for_each = local.mail_egress_verification
}

resource "hetznerdns_record" "spf" {
  zone_id = each.value.zone.id

  name  = "@"
  value = "\"${each.value.spf}\""
  type  = "TXT"
  ttl   = 600

  for_each = local.mail_egress_verification
}
