# DNS zones

This module contains the setup for our DNS zones. It should only contain the zones themselves. Having a separate module
containing just the zones introduces a bulkhead between the rest of our DNS configuration and the zones. This is useful
as destroying zones will take all DNS records with it and might lead to unexpected outages.

## Applying the configuration

There are corresponding CI jobs defined for this module.