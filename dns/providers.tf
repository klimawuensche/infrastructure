provider "hetznerdns" {
}

terraform {
  required_providers {
    hetznerdns = {
      source = "timohirt/hetznerdns"
      version = "1.2.0"
    }
  }
  backend "http" {
  }
}
