resource "hetznerdns_record" "mailfence_verification_klimabaender_de" {
  zone_id = hetznerdns_zone.klimabaender_de.id

  name  = "@"
  value = "chal=fbk0t0"
  type  = "TXT"
  ttl   = 600
}

resource "hetznerdns_record" "mailfence_verification_mail_klimabaender_de" {
  zone_id = hetznerdns_zone.klimabaender_de.id

  name  = "mail"
  value = "chal=a7450k"
  type  = "TXT"
  ttl   = 600
}

locals {
  mailfence_smtp_servers = ["smtp1.mailfence.com", "smtp2.mailfence.com"]
}

resource "hetznerdns_record" "mx_mail_klimabaender_de" {
  for_each = toset(local.mailfence_smtp_servers)

  zone_id = hetznerdns_zone.klimabaender_de.id
  name    = "mail"
  value   = "10 ${each.value}."
  type    = "MX"
  ttl     = 600
}

resource "hetznerdns_record" "mx_klimabaender_de" {
  for_each = toset(local.mailfence_smtp_servers)

  zone_id = hetznerdns_zone.klimabaender_de.id
  name    = "@"
  value   = "10 ${each.value}."
  type    = "MX"
  ttl     = 600
}

resource "hetznerdns_record" "dkim_mail_klimabaender_de" {
  zone_id = hetznerdns_zone.klimabaender_de.id

  name  = "20210709-9ce8._domainkey.mail"
  value = "\"v=DKIM1; k=rsa; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvSPtiaU01wyoD+NgIH+A8GWvbbMaql1WigIa3OmxNeqxc2uVLGBUUm47vpPPVl4cgnqOpCdDV2hx+tb7kLMqGjvJnT6X38XznlVIXgsc5e4iiCxo04lvaDZq7z/FNQBWYZ15C8hSGGsIM2i16j38PG7fzTAloPL+F6rutB1pkRz5L68/DUxeD8jXJMDFE2iPw\" \"/A6WF5QYtggkZh1uvmjTOPLHoJRXV5hpwb16ASAMNLdJD336B2y3yOps6DWPbF1lL0hqq2bh8ekCWlhjhkZylREra5jRWEvj25fvzdlsvja0grIC9IDINAwZKL45Yo2QalFXkDSgOmef2EFxoNYMwIDAQAB\" "
  type  = "TXT"
  ttl   = 600
}

resource "hetznerdns_record" "dkim_klimabaender_de" {
  zone_id = hetznerdns_zone.klimabaender_de.id

  name  = "20210713-5ryc._domainkey"
  value = "\"v=DKIM1; k=rsa; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzYZti3hH6wnVhAHpV+t0TCskmrANP7jEqOT1kjOephAQ4ecD6JLP06eLizjUCqnrL2bBUZbyFNBLPK5CF7lJfOkBRd0QkVcU1R1++0Z8Ncoi6KHMgVL2W//utjDjkf2+J1nca+rPKmul7L2+rTewMV2WnE+pGqb3iYnOFpyJpcteeuWrT9S0KButAx1ZYN5TK\" \"98Iv5L0/zaNZ7aDNalRxOuqGrSj1Yn8jTCY2fktyr0vXqWHxrjWoWbA2oDgfmufs//HrrwSqN/ehOR7v9czeZC8MvJhavrE+DNJLsstO4b6u3i+4ksFwlbNviK4m6G36OIUtjFrEDumHt62SeGctwIDAQAB\" "
  type  = "TXT"
  ttl   = 600
}

resource "hetznerdns_record" "dmarc_mail_klimabaender_de" {
  zone_id = hetznerdns_zone.klimabaender_de.id

  name  = "_dmarc.mail"
  value = "\"v=DMARC1; p=none; rua=mailto:reports@mail.klimabaender.de\""
  type  = "TXT"
  ttl   = 600
}

resource "hetznerdns_record" "spf_mail_klimabaender_de" {
  zone_id = hetznerdns_zone.klimabaender_de.id

  name  = "mail"
  value = "\"v=spf1 include:_spf.mailfence.com ~all\""
  type  = "TXT"
  ttl   = 600
}
