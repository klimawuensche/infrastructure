resource "hetznerdns_record" "sendinblue_verification" {
  zone_id = hetznerdns_zone.klimawuensche_de.id
  name = "@"
  value = "Sendinblue-code:5ce85b82f7de306f4ca7b6cd7635e019"
  type = "TXT"
  ttl= 600
}

resource "hetznerdns_record" "google_site_verification" {
  zone_id = hetznerdns_zone.klimawuensche_de.id
  name = "@"
  value = "google-site-verification=XB2HWn-mtJXoDBfjWjaXsEH6z5akVHSrESUNL51da68"
  type = "TXT"
  ttl= 600
}

resource "hetznerdns_record" "sendinblue_verification_klimabaender" {
  zone_id = hetznerdns_zone.klimabaender_de.id
  name = "@"
  value = "Sendinblue-code:5ce85b82f7de306f4ca7b6cd7635e019"
  type = "TXT"
  ttl= 600
}

resource "hetznerdns_record" "google_site_verification_klimabaender" {
  zone_id = hetznerdns_zone.klimabaender_de.id
  name = "@"
  value = "google-site-verification=DKGQTWXWDTch_7-E0KD9M_mrQUg4Ft0fmqJYKECUN3w"
  type = "TXT"
  ttl= 600
}
