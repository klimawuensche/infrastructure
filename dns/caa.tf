resource "hetznerdns_record" "caa" {
  zone_id = each.key
  name = "@"
  value = "0 issue \"letsencrypt.org\""
  type = "CAA"
  ttl= 3600

  for_each = toset(local.zones.*.id)
}

