# Klimabänder infrastructure setup

This repository contains the cloud infrastructure and configuration of the [Klimabänder page](https://www.klimabaender.de), along with
auxiliary infrastructure, as the CI server.

## Architecture

```mermaid
graph LR
    app-user -- TLS --> loadbalancer
    app-user -- TLS --> cdn
    app-user -- TLS --> klimabaender-static

    subgraph internal["Hetzner internal network"]
    loadbalancer
    app-0
    app-1
    ci
    app-0 -- TLS --> db
    app-1 -- TLS --> db
    end

    subgraph keycdn["keycdn"]
    cdn["cdn.klimabaender.de"]
    end

    cdn -- TLS --> klimabaender-images

    subgraph strato["Strato"]
    klimabaender-static
    klimabaender-images
    end

    loadbalancer -- "HTTP :(" --> app-0
    loadbalancer -- "HTTP :(" --> app-1
```

Traffic ingress is a Hetzner loadbalancer, which select our application servers via instance labels. The loadbalancer is reachable via public IP and distributes the traffic to the application servers.
We have at least two application servers, which enables zero-downtime deployments and zero-downtime instance reboots. The database is currently not highly available, as it is not replicated to multiple instances.

The loadbalancer also servers as TLS offloader. Unfortunately, this currently means that traffic from the loadbalancer to our application servers is unencrypted. However, the application ports are only reachable
from within the private network. This means that while the traffic is unencrypted, it only flows within our private network. The traffic from the application servers to the database is encrypted using TLS.

All infrastructure is defined as Infrastructure as Code, using [Terraform][1] and [Ansible][2]. The major terraform resources are managed via GitOps, meaning that there are CI jobs for provisioning the infrastructure,
as well as for rolling out changes. Ansible is still run manually, so far.

There are two object storage buckets provisioned with Strato HiDrive-S3. The bucket `klimabaender-static` contains large static content files, such as flyers or posters. The bucket `klimabaender-images` contains
mainly user-supplied images. In order to be able to dynamically rescale these images as needed, we provisioned a CDN zone at [keycdn][3], which is available under `cdn.klimabaender.de`. The CDN
is not managed via infrastructure as code. There is just a CNAME record pointing to our manually configured CDN zone.

## Contents

This repository contains definitions for:
- [application_server](application_server): the application server(s).
- [ci](ci): the CI server.
- [database_server](database_server) the database server.
- [dns](dns): the DNS zones and core resource records such as CAA, MX etc.
- [loadbalancer](loadbalancer): the main load balancer at Hetzner.
- [network](network): the internal network at Hetzner.
- [storage](storage): the object storage buckets at Strato.
- [tracking_server](tracking_server): the server running Matomo

## Authentication

The configuration in this repository needs credentials for our Hetzner account (for the actual cloud infrastructure),
as well as the Gitlab group (for the Terraform state). Supply them using the following environment variables:
- `GITLAB_USERNAME`: your Gitlab username
- `GITLAB_API_TOKEN`: a valid token with scope `api` for your Gitlab account
- `HCLOUD_TOKEN`: a valid token for our project at Hetzner
- `HETZNER_DNS_API_TOKEN`: a valid token for the Hetzner DNS API. Important: this needs to be a token owned by the owner of the Hcloud project.
All Ansible and Terraform directories contain Makefiles which will prompt for missing environment variables.

## Monitoring

General monitoring is done using [Datadog][4]. Datadog agents are deployed on the application and database servers.

Uptime monitoring is done via [uptimerobot][5]. You may find the current application status on our [status page][6].

[1]: https://terraform.io
[2]: https://ansible.com
[3]: https://www.keycdn.com
[4]: https://datadoghq.com
[5]: https://uptimerobot.com
[6]: https://stats.uptimerobot.com/QgPL4u6P1z
