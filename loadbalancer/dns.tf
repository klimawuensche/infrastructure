data "hetznerdns_zone" "klimawuensche_de" {
  name = "klimawuensche.de"
}

data "hetznerdns_zone" "klimawuensche_de_umlaut" {
  name = "xn--klimawnsche-yhb.de"  # klimawünsche.de
}

data "hetznerdns_zone" "klimawuensche_org_umlaut" {
  name = "xn--klimawnsche-yhb.org"  # klimawünsche.org
}

data "hetznerdns_zone" "klimawuensche_org" {
  name = "klimawuensche.org"
}

data "hetznerdns_zone" "klimawuensche_net" {
  name = "klimawuensche.net"
}

data "hetznerdns_zone" "klimabaender_de" {
  name = "klimabaender.de"
}

data "hetznerdns_zone" "klimabaender_de_umlaut" {
  name = "xn--klimabnder-v5a.de"  # klimabänder.de
}

data "hetznerdns_zone" "klimabaender_com" {
  name = "klimabaender.com"
}

data "hetznerdns_zone" "klimabaender_com_umlaut" {
  name = "xn--klimabnder-v5a.com"  # klimabänder.com
}

data "hetznerdns_zone" "klimabaender_org_umlaut" {
  name = "xn--klimabnder-v5a.org"  # klimabänder.org
}

locals {
  dns_zones = [
    data.hetznerdns_zone.klimawuensche_de,
    data.hetznerdns_zone.klimawuensche_de_umlaut,
    data.hetznerdns_zone.klimawuensche_org,
    data.hetznerdns_zone.klimawuensche_org_umlaut,
    data.hetznerdns_zone.klimawuensche_net,
    data.hetznerdns_zone.klimabaender_de,
    data.hetznerdns_zone.klimabaender_de_umlaut,
    data.hetznerdns_zone.klimabaender_com,
    data.hetznerdns_zone.klimabaender_com_umlaut,
    data.hetznerdns_zone.klimabaender_org_umlaut,
  ]
}

locals {
  dns_zones_by_name = {for zone in local.dns_zones : zone.name => zone}
}

resource "hetznerdns_record" "www" {
  zone_id = each.value.id
  name = "www"
  value = hcloud_load_balancer.frontend.ipv4
  type = "A"
  ttl= 300

  for_each = local.dns_zones_by_name
}

resource "hetznerdns_record" "second_level" {
  zone_id = each.value.id
  name = "@"
  value = hcloud_load_balancer.frontend.ipv4
  type = "A"
  ttl= 300

  for_each = local.dns_zones_by_name
}

resource "hetznerdns_record" "www_aaaa" {
  zone_id = each.value.id
  name = "www"
  value = hcloud_load_balancer.frontend.ipv6
  type = "AAAA"
  ttl= 300

  for_each = local.dns_zones_by_name
}

resource "hetznerdns_record" "second_level_aaaa" {
  zone_id = each.value.id
  name = "@"
  value = hcloud_load_balancer.frontend.ipv6
  type = "AAAA"
  ttl= 300

  for_each = local.dns_zones_by_name
}

