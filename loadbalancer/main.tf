data "hcloud_network" "private" {
  name = "network"
}

resource "hcloud_load_balancer" "frontend" {
  name               = "frontend"
  load_balancer_type = "lb11"
  location           = "nbg1"

  lifecycle {
    prevent_destroy = true
  }
}

resource "hcloud_load_balancer_network" "srvnetwork" {
  load_balancer_id = hcloud_load_balancer.frontend.id
  network_id = data.hcloud_network.private.id
}

