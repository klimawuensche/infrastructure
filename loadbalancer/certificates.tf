resource "hcloud_managed_certificate" "certificate" {
  name = each.key
  domain_names = [
    each.key,
    "www.${each.key}",
  ]

  for_each = local.dns_zones_by_name
}
