resource "hcloud_load_balancer_service" "load_balancer_service" {
  load_balancer_id = hcloud_load_balancer.frontend.id
  protocol = "https"

  http {
    certificates = [for cert in hcloud_managed_certificate.certificate: cert.id]
    redirect_http = true
  }

  health_check {
    interval = 3  # minimum seems to be 3
    port = 80
    protocol = "http"
    timeout = 2
    retries = 1

    http {
      domain = "www.klimabaender.de"
      path = "/healthcheck"
      status_codes = ["200"]
    }
  }
}

resource "hcloud_load_balancer_target" "application_servers" {
  type             = "label_selector"
  load_balancer_id = hcloud_load_balancer.frontend.id
  label_selector   = "Role=application_server"
  use_private_ip   = true
}
