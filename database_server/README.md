# CI server

The [terraform](terraform) directory contains the Terraform definitions for an instance intended as an application server for the project.
The [ansible](ansible) directory contains an Ansible playbook which configures the instance (but does not yet run the application).
In order to actually deploy the application, you need to trigger a build in the [website](https://gitlab.com/klimawuensche/website) project.

## Applying the configuration

If you want to start from scratch, or ensure that the current configuration is applied, just run
```shell
make apply
```

## Vertical scaling

In order to change the instance type of the database, just modify the variable `volume_name` in the [vars.tf](terraform/volume/vars.tf)
to the instance size you want to scale to, and run `make apply`. Note that this will result in a loss in database
connectivity of roughly 30-60 seconds. After that, the database should come up again. The applications will reconnect on their own.