provider "hcloud" {
}

terraform {
  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "~> 1.26"
    }
  }
  backend "http" {
  }
}
