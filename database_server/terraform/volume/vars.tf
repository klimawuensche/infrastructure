variable "volume_name" {
  description = "The (hetzner-internal) volume name of the volume"
  default = "klima_database"
}

variable "volume_size" {
  description = "The volume size (in GB) of the volume. Note that increasing this might mean manually extending the disk file system"
  default = 10
}