variable "hcloud_location" {
  description = "The location to use for the infrastructure components"
  default = "nbg1"
}