resource "hcloud_volume" "klima_database" {
  name = var.volume_name
  size = var.volume_size
  location = var.hcloud_location

  lifecycle {
    prevent_destroy = true
  }
}