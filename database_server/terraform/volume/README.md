# Data volume

This Terraform module provisions a data volume for use in a cloud server.

Apply the configuration with
```shell
make apply
```
or destroy the infrastructure using
```shell
make destroy
```
Note that the volume is protected using a `lifecycle` directive. You will need to set `prevent_destroy` to `false` before
destruction.

## Configuration

The configuration variables reside in [vars.tf](vars.tf).