variable "klima_db_image" {
  description = "The OS image of the klima instance"
  default = "ubuntu-20.04"
}

variable "klima_db_server_type" {
  description = "The server type of the klima instance"
  default = "cx11"
}

variable "klima_db_ssh_keys" {
  description = "The names of the SSH keys to inject into the klima instance"
  default = ["diskordanz", "mgottschalk@gmx.net", "marco@ino.io", "ci-server"]
}

variable "hcloud_location" {
  description = "The location to use for the infrastructure components"
  default = "nbg1"
}
