# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hetznercloud/hcloud" {
  version     = "1.26.0"
  constraints = "~> 1.26"
  hashes = [
    "h1:2LLe4UKLS7R+t+tQL1oOFLA8c8/rs3iCfT26LyiQcsk=",
    "zh:03d7eb722a4ee25774949baace0125392060d0369d4cb9257d7d298ab6ece3ff",
    "zh:0fed2e63ac4cb6fe6b2a5b6891abf973cb7c1716e487fbabc09216e0ec05e866",
    "zh:1a84c8c1c8e2d6607de5aa09aa3f9254183cde75a5acc666cca5f4b02a1d290e",
    "zh:23ac426aa3a0001fb20045dc35569978864f139732f45ab671c64e80123c91a1",
    "zh:23b78348b24ae3e4679bd90989c999346efd71ee228d17368d5f556f63e5fd06",
    "zh:2503fe28ac87661af96e7755a7404307000822104ac1abc571271eee46c95ab5",
    "zh:3fe859b2611d20ed5cd65cc2ec812acf73c7dfb39f2fee45ef99a3896c2662a8",
    "zh:51ef869ed35d0d8aada35f587c4a64802f1140dc93c40a4e7c9800560143bb1a",
    "zh:69b93cf4adca465b89da08e4e3b4aaf831821f1fbae68e526c0a292b3cfa463d",
    "zh:6a4e23c6aa86e3d30240e6e4c97daef3af9ad217be2c6f35300fe1839fdbf8b2",
    "zh:97a513459692a981a62b4a566c1d736c4a67622d2fbbee3771ec3ea8d576d484",
    "zh:fec6c07731e23d1dd45015b44747b89c4fee58b5b2560f96d24c7da5a8ecb2ad",
  ]
}

provider "registry.terraform.io/timohirt/hetznerdns" {
  version     = "1.2.0"
  constraints = "1.2.0"
  hashes = [
    "h1:4c5OctdietH//b/7MiyDTOOYNK8LaaDLhNoTw4JsEe8=",
    "h1:HFSfVX2yoeF0Zv9+Yh1TVgBu6TqpvSd9b5UuTIfTOvE=",
    "h1:J3xGHuN9jXCOjwmdWBXhjkUHP3DdovjzwpUwbEvM5GU=",
    "h1:P0fTzuBQf5dDVDv0D+u2g9HNXKGwCPi3TT+8ELvzZrA=",
    "h1:eeWcZfXY4Aqj+6FZoJHTVx5ombEL9ncWn/1S0muWf1Q=",
    "h1:lu0RZ+1RFH1j14NXfl/gBDPv4yW2ExoUeGUqKT9QD7g=",
    "h1:tZ31LvG/IMBhjx5PDnBHz4AsRf7tEW1iglh+Asy3NnM=",
    "h1:uzMH8VxS1WhKVK5l+jCGj/aJhIcvnj69DQqh1MKV8FQ=",
  ]
}
