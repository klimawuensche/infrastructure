# Data volume

This Terraform module provisions:
- a virtual server instance
- a firewall allowing only SSH, HTTP and HTTPS
- a floating IP pointing to the server instance
- a volume attachment for the data volume

The floating IP is provided so that we can arbitrarily recreate the server without having to update the DNS A record, as
the DNS zone is not handled at Hetzner. There currently is just an IPv4 IP, but there is no real reason not to add an IPv6
IP (besides the additional monthly expense).

Apply the configuration using the CI pipelines.

## Configuration

The configuration variables reside in [vars.tf](vars.tf).