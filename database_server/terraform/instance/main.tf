resource "hcloud_server" "klima_database" {
  name = "database"
  image = var.klima_db_image
  server_type = var.klima_db_server_type
  ssh_keys = var.klima_db_ssh_keys
  location = var.hcloud_location
  user_data = templatefile("${path.module}/user_data.tpl", {username = "klima"})
  backups = false
  keep_disk = true  # If true, do not upgrade the disk. This allows downgrading the server type later.
  firewall_ids = [hcloud_firewall.firewall.id]

  labels = {
    Name = "db"
    Role = "database_server"
    DataDevice = reverse(split("/", data.hcloud_volume.klima_database.linux_device))[0]
  }
}

resource "hcloud_firewall" "firewall" {
  name = "klima_db_firewall"

  rule {
    direction = "in"
    protocol = "tcp"
    port = "22"
    source_ips = [
      "0.0.0.0/0",
      "::/0"
    ]
  }

  rule {
    direction = "in"
    protocol = "tcp"
    port = "27017"
    source_ips = [
      "10.0.0.0/24"
    ]
  }
}

resource "hcloud_volume_attachment" "klima_database" {
  volume_id = data.hcloud_volume.klima_database.id
  server_id = hcloud_server.klima_database.id
  automount = false
}

data "hcloud_volume" "klima_database" {
  name = "klima_database"
}

data "hetznerdns_zone" "klimawuensche_de" {
  name = "klimawuensche.de"
}

resource "hetznerdns_record" "db" {
  zone_id = data.hetznerdns_zone.klimawuensche_de.id
  name = "db"
  value = hcloud_server_network.db.ip
  type = "A"
  ttl= 300
}
