output "public_ipv4" {
  value = hcloud_server.klima_database.ipv4_address
}

output "public_ipv6" {
  value = hcloud_server.klima_database.ipv6_address
}

output "data_device" {
  value = data.hcloud_volume.klima_database.linux_device
}