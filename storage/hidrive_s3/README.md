# HiDrive S3 object storage

This Ansible playbook provisions object storage buckets with Strato HiDrive-S3. Although we try to use Terraform where
possible, the buckets are managed using Ansible. This is because the AWS S3 Terraform provider is not compatible with
Strato HiDrive-S3, as it uses some API calls not implemented by Strato, such as `GetBucketVersioning`.
Ansible is more forgiving in this regard.

## Prerequisites

In contrast to the other Terraform/Ansible configuration in this repository, this playbook only needs two environment
variables in order to work:
- `AWS_ACCESS_KEY_ID`: The access key ID provided by Strato
- `AWS_SECRET_ACCESS_KEY`: The secret access key provided by Strato

## Applying the configuration

```bash
make install  # you only need to run this once to install the necessary Ansible AWS collection and Python packages
make apply
```